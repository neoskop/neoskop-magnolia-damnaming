# README

Dieses Modul liefert einen Patch für das inkonsistente Umbenennen von Assets in Magnolia 5 (Assets speichern seit Magnolia 5 neben dem JCR-Node-Name auch noch einen gesonderten Asset-Namen).

Der Patch stellt sicher, dass der JCR-Node-Name und der Asset-Name nach einer Umbenennung synchron sind.

## Installation

Als Repository muss [JitPack](https://jitpack.io/) in der `pom.xml` des Magnolia-Projektes hinzugefügt werden:

```xml
<repositories>
	<repository>
		<id>jitpack.io</id>
		<url>https://jitpack.io</url>
	</repository>
</repositories>
```

Das Modul muss dann in der `pom.xml` des Magnolia-Projektes als Abhängigkeit hinzugefügt werden:

```xml
<dependency>
	<groupId>org.bitbucket.neoskop</groupId>
	<artifactId>neoskop-magnolia-damnaming</artifactId>
	<version>1.1.2</version>
</dependency>
```

## Abhängigkeiten

Das Module benötigt Magnolia >= 5.3.8 und DAM >= 2.0.8 ab Version 1.0.6.

Bis Version 1.1.2:

- [Magnolia CMS][1] >= 5.3.8

Ab Version 1.2.0:

- [Magnolia CMS][1] >= 5.6.7