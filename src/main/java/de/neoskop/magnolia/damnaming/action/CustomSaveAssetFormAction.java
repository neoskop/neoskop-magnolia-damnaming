package de.neoskop.magnolia.damnaming.action;

import com.vaadin.v7.data.Item;
import info.magnolia.dam.app.assets.form.action.SaveAssetFormAction;
import info.magnolia.dam.jcr.AssetNodeTypes;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.objectfactory.Components;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.form.action.SaveFormActionDefinition;
import info.magnolia.ui.vaadin.integration.jcr.JcrNewNodeAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import org.apache.commons.lang3.StringUtils;

public class CustomSaveAssetFormAction extends SaveAssetFormAction {

  public CustomSaveAssetFormAction(
      SaveFormActionDefinition definition,
      Item item,
      EditorCallback callback,
      EditorValidator validator) {
    super(definition, item, callback, validator);
  }

  @Override
  protected void setNodeName(Node assetNode, JcrNodeAdapter item) throws RepositoryException {
    String newNodeName;
    Node resourceNode = AssetNodeTypes.AssetResource.getResourceNodeFromAsset(assetNode);

    if (item instanceof JcrNewNodeAdapter) {
      newNodeName = generateUniqueNodeNameForAsset(assetNode, extractFileName(resourceNode));
    } else if (assetNode.hasProperty(AssetNodeTypes.Asset.ASSET_NAME)) {
      Property assetName = assetNode.getProperty(AssetNodeTypes.Asset.ASSET_NAME);
      newNodeName =
          Components.getComponent(NodeNameHelper.class).getValidatedName(assetName.getString());
    } else {
      newNodeName =
          Components.getComponent(NodeNameHelper.class)
              .getValidatedName(extractFileName(resourceNode));
    }

    if (StringUtils.isNotBlank(newNodeName)) {
      assetNode.setProperty("name", newNodeName);
      item.setNodeName(newNodeName);
      NodeUtil.renameNode(assetNode, newNodeName);
    }
  }
}
