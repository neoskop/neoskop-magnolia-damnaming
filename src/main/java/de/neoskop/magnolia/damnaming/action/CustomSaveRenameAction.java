package de.neoskop.magnolia.damnaming.action;

import com.vaadin.v7.data.Item;
import info.magnolia.dam.jcr.AssetNodeTypes;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.objectfactory.Components;
import info.magnolia.ui.dialog.action.SaveDialogAction;
import info.magnolia.ui.dialog.action.SaveDialogActionDefinition;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

public class CustomSaveRenameAction<T extends SaveDialogActionDefinition>
    extends SaveDialogAction<T> {

  public CustomSaveRenameAction(
      T definition, Item item, EditorValidator validator, EditorCallback callback) {
    super(definition, item, validator, callback);
  }

  @Override
  protected void setNodeName(Node node, JcrNodeAdapter item) throws RepositoryException {
    String newNodeName =
        Components.getComponent(NodeNameHelper.class).getValidatedName(node.getName());
    item.setNodeName(newNodeName);
    NodeUtil.renameNode(node, newNodeName);

    if (node.hasProperty(AssetNodeTypes.Asset.ASSET_NAME)) {
      node.setProperty(AssetNodeTypes.Asset.ASSET_NAME, newNodeName);
    }
  }
}
