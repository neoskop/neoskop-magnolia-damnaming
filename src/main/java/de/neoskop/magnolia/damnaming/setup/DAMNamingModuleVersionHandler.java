package de.neoskop.magnolia.damnaming.setup;

import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.CheckOrCreatePropertyTask;
import info.magnolia.module.delta.ModuleBootstrapTask;
import info.magnolia.module.delta.NodeExistsDelegateTask;
import info.magnolia.module.delta.Task;
import java.util.ArrayList;
import java.util.List;

public class DAMNamingModuleVersionHandler extends DefaultModuleVersionHandler {
  protected List<Task> getStartupTasks(InstallContext installContext) {
    List<Task> tasks = new ArrayList<Task>(super.getStartupTasks(installContext));
    tasks.add(new ModuleBootstrapTask());
    tasks.add(getAssetFieldReadonlyTask());
    return tasks;
  }

  private Task getAssetFieldReadonlyTask() {
    final String nodePath = "/modules/dam-app/dialogs/renameAsset/form/tabs/item/fields/assetName";
    final String propertyName = "readOnly";
    Task propertyCreationTask =
        new CheckOrCreatePropertyTask(
            "Create or set readOnly property", nodePath, propertyName, "true");
    return new NodeExistsDelegateTask("Check asset field exists", nodePath, propertyCreationTask);
  }
}
